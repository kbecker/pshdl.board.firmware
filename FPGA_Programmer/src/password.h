/*
 * password.h
 *
 * Created: 03.05.2014 16:41:16
 *  Author: Yaro
 */ 


#ifndef PASSWORD_H_
#define PASSWORD_H_

#include <asf.h>
#include <stdio.h>
#include <string.h>

#include "udi_cdc.h"

#define USB_PASSWORD "PSHDLBoardUnlock"

void waitUntilUsbPasswordReceived(void);

#endif /* PASSWORD_H_ */