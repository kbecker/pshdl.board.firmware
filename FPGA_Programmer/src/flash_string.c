/*
 * flash_string.c
 *
 * Created: 11.10.2013 10:25:54
 *  Author: gevorkov
 */ 

#include "flash_string.h"

const PROGMEM char tooLongStringErrorMessage[] = "\nFLASH_STRING_BUFFER_SIZE too small\n";

char* getStringFromFlash(PGM_P str, uint8_t strLength){
	if(NVM.CMD == 0x00){
		if(strLength < FLASH_STRING_BUFFER_SIZE){
			return strcpy_P( flashStringBuffer , str);
		}else{
			return strcpy_P( flashStringBuffer , tooLongStringErrorMessage);
		}
	}else{
		return strcpy_P( flashStringBuffer , PSTR("\nFlash String error: NVM busy => one string will be discarded\n"));
	}
}