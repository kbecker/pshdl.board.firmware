/*
 * clockOutput.c
 *
 * Created: 28.11.2013 20:16:40
 *  Author: Yaro
 */ 
#include "clockOutput.h"

void enableClockOutputOnPC4(void){
	PORTCFG.CLKEVOUT = PORTCFG_CLKEVPIN_PIN4_gc | PORTCFG_CLKOUTSEL_CLK1X_gc | PORTCFG_CLKOUT_PC7_gc;
	PORTC.DIRSET = PIN4_bm;
}