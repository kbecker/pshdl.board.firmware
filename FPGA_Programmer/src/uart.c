/*
 * uart.c
 *
 * Created: 24.11.2013 21:18:23
 *  Author: Yaro
 */ 
#include "uart.h"

uint16_t transmitBufferMinLeftSpace;


void initUART(uint32_t baudrate){
	transmitBufferMinLeftSpace = 0;
	
	PORTC.DIRCLR = CTS_PIN_bm; //CTS
	PORTC.DIRSET = RTS_PIN_bm; //RTS
	
	usart_serial_options_t usart_options = {
		.baudrate = baudrate,
		.charlength = USART_CHSIZE_8BIT_gc,
		.paritytype = USART_PMODE_DISABLED_gc,
		.stopbits = false
	};

	
	usart_serial_init(&USARTC1, &usart_options);
	
	//tx output
	PORTC.DIRSET = PIN7_bm;
	
	//enable uart receive interrupt
	USARTC1.CTRLA |= USART_RXCINTLVL_LO_gc;

	setRTS();
}	

void deInitUART(void){
	clearRTS();
	
	PORTC.DIRCLR = RTS_PIN_bm; //RTS
	
	USARTC1.CTRLA = 0;
	USARTC1.CTRLB = 0;
	USARTC1.CTRLC = 0;
	USARTC1.BAUDCTRLA = 0;
	USARTC1.BAUDCTRLB = 0;	
}

ISR(USARTC1_RXC_vect){
	uint8_t data = USARTC1.DATA;
	
	if (transmitBufferMinLeftSpace == 0){
		clearRTS();
		do{
			transmitBufferMinLeftSpace = udi_cdc_multi_get_free_tx_buffer(0);
		}while(transmitBufferMinLeftSpace == 0);
		setRTS();
	}
	
	udi_cdc_multi_putc_lite(0, data);
}

volatile int i = 0;
void handleDataLinkerUART(void){
	uint32_t byteCount;
	
	do{
		byteCount = usb_getUint32(USB_DATA_LINKER_PORT);
		//usb_putUint32(USB_DATA_LINKER_PORT, byteCount);

		uint32_t bytesLeft = byteCount;

		while (bytesLeft != 0) {
			uint32_t processDataPackageBytes;
			if (bytesLeft <= DATA_LINKER_PACKET_SIZE) {
				processDataPackageBytes = bytesLeft;
				} else {
				processDataPackageBytes = DATA_LINKER_PACKET_SIZE;
			}

			if(!(processDataPackageBytes%64)) { //workaround for bug in usb lib
				processDataPackageBytes--;
			}
		
		
			uint32_t processDataPackageBytesCopy = processDataPackageBytes; //workaround for bug in usb lib
		
			while(processDataPackageBytesCopy!=0) {
				uint32_t processBytes;
				if (processDataPackageBytesCopy <= DATA_LINKER_BUF_SIZE) {
					processBytes = processDataPackageBytesCopy;
					} else {
					processBytes = DATA_LINKER_BUF_SIZE;
				}
			
				udi_cdc_multi_read_buf(USB_DATA_LINKER_PORT, dataLinkerBuf, processBytes);
				usart_serial_write_packet(&USARTC1, dataLinkerBuf, processBytes);
				//udi_cdc_multi_write_buf(0, dataLinkerBuf, processBytes); //debug!!!!!!!!!!!!!!!!!!!!!!!!

				processDataPackageBytesCopy -= processBytes;
			}

			bytesLeft -= processDataPackageBytes;
		}
	}while (byteCount != 0);

}