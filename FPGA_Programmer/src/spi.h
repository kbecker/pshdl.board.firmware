/*
 * spi.h
 *
 * Created: 08.11.2013 15:30:26
 *  Author: Yaro
 */ 


#ifndef SPI_H_
#define SPI_H_

#include <asf.h>
#include <stdio.h>
#include <string.h>
#include "PCInteractions.h"
#include "dataLinker.h"

void initSPIMaster(uint8_t prescaler);
void deInitSPIMaster(void);
void selectSPISlaveFPGA(void);
void deSelectSPISlaveFPGA(void);

void disableSPIInterrupt(void);
void blockingBufferTransferAndReceiveSPI(uint8_t* buffer, uint8_t byteCount);
void blockingBufferTransferSPI(uint8_t* buffer, uint8_t byteCount);
void blockingBufferReceiveSPI(uint8_t* buffer, uint8_t sendChar, uint8_t byteCount);

void handleDataLinkerSPI(void);

#endif /* SPI_H_ */