/*
 * PCInteractions.c
 *
 * Created: 04.11.2013 22:26:43
 *  Author: Yaro
 */ 

#include "PCInteractions.h"

uint32_t usb_getUint32(uint8_t port){
	uint32_t data = 0;
	uint8_t dataPart;
	
	for (uint8_t i = 0; i < 4; i++) {
		dataPart = udi_cdc_multi_getc(port);
		data |= (uint32_t)dataPart << i * 8;
	}
	
	return data;
}

uint16_t usb_getUint16(uint8_t port){
	uint16_t data = 0;
	uint8_t dataPart;
	
	for (uint8_t i = 0; i < 2; i++) {
		dataPart = udi_cdc_multi_getc(port);
		data |= (uint16_t)dataPart << i * 8;
	}
	
	return data;
}

void usb_putUint32(uint8_t port, uint32_t data){
	for(int i = 0; i < 4; i++){
		udi_cdc_multi_putc(port, data >> i*8);
	}
}

void usb_putUint16(uint8_t port, uint16_t data){
	for(int i = 0; i < 2; i++){
		udi_cdc_multi_putc(port, data >> i*8);
	}
}