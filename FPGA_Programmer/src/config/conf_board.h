#ifndef CONF_BOARD_H
#define CONF_BOARD_H

//software JTAG for programming FPGA via LCPS
//#define LCPS_Port PORTC         //LCPS Port darf nicht f�r andere Sachen verwendet werden
//
//#define LCPS_TCK_bm     PIN0_bm	
//#define LCPS_TDI_bm     PIN1_bm	
//#define LCPS_TMS_bm     PIN3_bm	
//#define LCPS_TRST_bm    PIN2_bm	
//#define LCPS_TDO_bm     PIN4_bm	

#define LCPS_Port PORTD         //LCPS Port darf nicht f�r andere Sachen verwendet werden

#define LCPS_TCK_bm     PIN0_bm
#define LCPS_TDI_bm     PIN1_bm
#define LCPS_TMS_bm     PIN3_bm
#define LCPS_TRST_bm    PIN2_bm
#define LCPS_TDO_bm     PIN4_bm


#endif // CONF_BOARD_H
