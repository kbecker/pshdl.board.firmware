/****************************************************************************/
/*                                                                          */
/*  DirectC			        Copyright (C) Actel Corporation 2007            */
/*  Version 3.0     Release date September 30, 2013                         */
/*                                                                          */
/****************************************************************************/
/*                                                                          */
/*  Module:         dpuser.c												*/
/*                                                                          */
/*  Description:    user specific file containing JTAG interface functions  */
/*                  and delay function                                      */
/*                                                                          */
/****************************************************************************/

#include "dpuser.h"
#include "dpalg.h"
#include "dputil.h"

/* 
 * User attention:
 * Include files needed to support hardware JTAG interface operations.
 * 
*/
#include "DirectC_PC_Interaction.h"


/* This variable is used to select external programming vs IAP programming */
DPUCHAR hardware_interface = GPIO_SEL;
DPUCHAR enable_mss_support = FALSE;

/*
 * User attention:
 * jtag_port_reg: 	8 bit Static variable to keep track of the state of all the JTAG pins 
 * 					at all times during the programming operation.
 * Note: User check the variable size to make sure it can cover the hardware IO register. 
 * 
*/
static DPUCHAR jtag_port_reg; 


/*
 * User attention: 
 * Module: jtag_inp
 * 		purpose: report the logic state of tdo jtag pin
 * Arguments: None
 * Return value: 8 bit value
 * 		0, 0x80
 * 
*/
DPUCHAR jtag_inp(void)
{
	return (LCPS_Port.IN & LCPS_TDO_bm) ?  0x80 : 0;
}
/*
 * User attention: 
 * Module: jtag_outp
 * 		purpose: Set the JTAG port (all JTAG pins)
 * Arguments: 8 bit value containing the new state of all the JTAG pins
 * Return value: None
 * 
*/

//defined as inline in header!
// inline void jtag_outp(DPUCHAR outdata)
// {	
// // 	uint8_t temp = LCPS_Port.OUT;
// // 	
// // 	temp &= ~ (LCPS_TCK_bm | LCPS_TMS_bm | LCPS_TRST_bm | LCPS_TDI_bm);
// // 	
// // 	temp |= outdata & (LCPS_TCK_bm | LCPS_TMS_bm | LCPS_TRST_bm | LCPS_TDI_bm);
// 	
// //	LCPS_Port.OUT = temp;
// 	
// 	LCPS_Port.OUT = outdata;
// }

/*
 * No need to change this function
 * Module: dp_jtag_init
 * 		purpose: Set tck and trstb pins to logic level one
 * Arguments:
 * 		None
 * Return value: None
 * 
*/
void dp_jtag_init(void)
{
	jtag_port_reg = TCK | TRST; 
	jtag_outp(jtag_port_reg);
}

/*
 * No need to change this function
 * Module: dp_jtag_tms
 * 		purpose: Set tms pin to a logic level one or zero and pulse tck.
 * Arguments: 
 * 		tms: 8 bit value containing the new state of tms
 * Return value: None
 * Constraints: Since jtag_outp function sets all the jtag pins, jtag_port_reg is used 
 * 				to modify the required jtag pins and preseve the reset.
 * 
*/
void dp_jtag_tms(DPUCHAR tms)		 
{	
	jtag_port_reg &= ~(TMS | TCK);
	jtag_port_reg |= (tms ? TMS : 0);
	jtag_outp(jtag_port_reg);
	jtag_port_reg |= TCK;
	jtag_outp(jtag_port_reg);
}

/*
 * No need to change this function
 * Module: dp_jtag_tms_tdi
 * 		purpose: Set tms amd tdi pins to a logic level one or zero and pulse tck.
 * Arguments: 
 * 		tms: 8 bit value containing the new state of tms
 * 		tdi: 8 bit value containing the new state of tdi
 * Return value: None
 * Constraints: Since jtag_outp function sets all the jtag pins, jtag_port_reg is used 
 * 				to modify the required jtag pins and preseve the reset.
 * 
*/
void dp_jtag_tms_tdi(DPUCHAR tms, DPUCHAR tdi)
{
	jtag_port_reg &= ~(TMS | TCK | TDI);
	jtag_port_reg |= ((tms ? TMS : 0) | (tdi ? TDI : 0));
	jtag_outp(jtag_port_reg);
	jtag_port_reg |= TCK;
	jtag_outp(jtag_port_reg);
}

/*
 * No need to change this function
 * Module: dp_jtag_tms_tdi_tdo
 * 		purpose: Set tms amd tdi pins to a logic level one or zero, 
 * 				 pulse tck and return tdi level
 * Arguments: 
 * 		tms: 8 bit value containing the new state of tms
 * 		tdi: 8 bit value containing the new state of tdi
 * Return value: 
 * 		ret: 8 bit variable ontaining the state of tdo.
 * Valid return values: 
 * 		0x80: indicating a logic level high on tdo
 * 		0: indicating a logic level zero on tdo
 * Constraints: Since jtag_outp function sets all the jtag pins, jtag_port_reg is used 
 * 				to modify the required jtag pins and preseve the reset.
 * 
*/
DPUCHAR dp_jtag_tms_tdi_tdo(DPUCHAR tms, DPUCHAR tdi)
{
	DPUCHAR ret = 0x80;
	jtag_port_reg &= ~(TMS | TCK | TDI);
	jtag_port_reg |= ((tms ? TMS : 0) | (tdi ? TDI : 0));
	jtag_outp(jtag_port_reg);
	ret = jtag_inp() ;
	jtag_port_reg |= TCK;
	jtag_outp(jtag_port_reg);
	return ret;
}

/*
 * User attention: 
 * Module: dp_delay
 * 		purpose: Execute a time delay for a specified amount of time.
 * Arguments: 
 * 		microseconeds: 32 bit value containing the amount of wait time in microseconds.
  * Return value: None
 * 
*/
void dp_delay(DPULONG microseconds)
{
	delay_us(microseconds);
}

#ifdef ENABLE_DISPLAY

void dp_display_std_message(uint8_t messageNumber){
	directCSendCommandToPC(Programmer_Command_PRINT_STD_MESSAGE);
	udi_cdc_multi_putc(USB_DIRECTC_PORT, messageNumber);
}


void dp_display_text_debug(DPCHAR *text)
{
	uint8_t characterCount;
	
	for(characterCount = 0; text[characterCount] != 0; characterCount++);
	
	//sendCommandToPC(Programmer_Command_PRINT_STRING);
	//udi_cdc_multi_putc(USB_DIRECTC_PORT, characterCount);
	
	if(UDI_CDC_PORT_NB == 2){
		udi_cdc_multi_write_buf(1, text, characterCount);
	}else{
		dp_display_text(text);
	}
}


void dp_display_value_debug(DPULONG value,int descriptive)
{
	char buf[14];
	
	switch (descriptive){
		case HEX:
		sprintf(buf, "%lx", value);
		dp_display_text_debug(buf);
		break;
		
		case DEC:
		sprintf(buf, "%lu", value);
		dp_display_text_debug(buf);
		break;
		
		case CHR:
		sprintf(buf, "%c", (char)value);
		dp_display_text_debug(buf);
		break;
		
		default:
		sprintf(buf, "error 92213");
		dp_display_text_debug(buf);
	}
}


void dp_display_text(DPCHAR *text)
{
	uint8_t characterCount;
	
	for(characterCount = 0; text[characterCount] != 0; characterCount++);
	
	directCSendCommandToPC(Programmer_Command_PRINT_STRING);
	udi_cdc_multi_putc(USB_DIRECTC_PORT, characterCount);
	
    udi_cdc_multi_write_buf(USB_DIRECTC_PORT, text, characterCount);
}


void dp_display_value(DPULONG value,int descriptive)
{
	char buf[14];
	
	switch (descriptive){
		case HEX:
			sprintf(buf, "%lx", value);
			dp_display_text(buf);
			break;
		
		case DEC: 
			sprintf(buf, "%lu", value);
			dp_display_text(buf);
			break;
		
		case CHR:
			sprintf(buf, "%c", (char)value);
			dp_display_text(buf);
			break;
		
		default:
			sprintf(buf, "error 92213");
			dp_display_text(buf);
	}
}

void dp_display_array(DPUCHAR *value,int bytes, int descriptive)
{		
	for (int i = 0; i < bytes; i++){
		dp_display_value(value[i], descriptive);
		dp_display_value(' ', CHR);
	}
}
#endif
