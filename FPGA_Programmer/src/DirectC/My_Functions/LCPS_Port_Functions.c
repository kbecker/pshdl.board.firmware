/*
 * LCPS_Port_Functions.c
 *
 * Created: 09.10.2013 11:24:20
 *  Author: gevorkov
 */ 

#include "LCPS_Port_Functions.h"


void initLCPSPort(void){
	LCPS_Port.DIR = (LCPS_Port.DIR & ~LCPS_TDO_bm) | LCPS_TCK_bm | LCPS_TMS_bm | LCPS_TRST_bm | LCPS_TDI_bm;
}

void releaseLCPSPort(void){
	LCPS_Port.DIR &= ~(LCPS_TDO_bm | LCPS_TCK_bm | LCPS_TMS_bm | LCPS_TRST_bm | LCPS_TDI_bm);
}