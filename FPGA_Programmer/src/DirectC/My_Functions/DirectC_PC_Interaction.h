/*
 * DirectC_PC_Interaction.h
 *
 * Created: 18.10.2013 18:01:50
 *  Author: Yaro
 */ 


#ifndef PC_INTERACTION_H_
#define PC_INTERACTION_H_

#include "udi_cdc.h"
#include "conf_directC.h"

#include "dpuser.h"
#include "dpcom.h"
#include "LCPS_Port_Functions.h"
#include "dpalg.h"

void directCSendCommandToPC(programmerCommand command);
void handleProgrammerMode(void);

#endif /* PC_INTERACTION_H_ */