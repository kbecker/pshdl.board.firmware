/**
 * \file
 *
 * \brief User board initialization template
 *
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>

void board_init(void)
{
	PORTC.PIN2CTRL = PORT_OPC_PULLUP_gc; //SS, CTS
	PORTC.PIN7CTRL = PORT_OPC_PULLUP_gc; //TXD
	PORTC.PIN5CTRL = PORT_OPC_PULLDOWN_gc; //RTS
}
